#include <ESP8266WiFi.h>
#include <ESP8266WiFiMulti.h>
#include <WiFiClient.h>
#include <WiFiClientSecure.h>
#include <WiFiServer.h>
#include <WiFiServerSecure.h>
#include <WiFiUdp.h>

#include <DallasTemperature.h>
#include <OneWire.h>

char server[] = "Smtp.gmail.com"; //Chooce e-mail service
#define ONE_WIRE_BUS D1

OneWire oneWire(ONE_WIRE_BUS);

DallasTemperature sensors(&oneWire);

byte sendEmail(int t);
byte eRcv(WiFiClientSecure client);

const char* ssid = "NetworkNamepassword";
const char* password = "Network";
const char* user_base64 = "Email name in hash";
const char* user_password_base64 = "Email password in Hash";
const char* from_email = "MAIL From: <E-mail from>";  // Send e-mail from. Enable conection from devices
const char* to_email = "RCPT TO: <E-mail from to>";  // Recipient

const char* host = "www.dweet.io";
const char* thing  = "ThingName";  // Thing name
const char* thing_content = "Temperature";  // Send data
const char* thing_content1 = "Time";

char HH = 2; // GMT +2 +Summer time
char MM = 0;
unsigned int localPort = 2390;      // local port to listen for UDP packets

float t; // temperature
String timenow = "N/A";

IPAddress timeServerIP; // time.nist.gov NTP server address
const char* ntpServerName = "time.nist.gov";
const int NTP_PACKET_SIZE = 48; // NTP time stamp is in the first 48 bytes of the message
byte packetBuffer[ NTP_PACKET_SIZE]; //buffer to hold incoming and outgoing packets
// A UDP instance to let us send and receive packets over UDP
WiFiUDP udp;

WiFiClient client;

void setup(void)
{
  Serial.begin(115200);
  sensors.begin();

  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.print("IPess: ");
  Serial.println(WiFi.localIP());

  Serial.println("Starting UDP");
  udp.begin(localPort);
  Serial.print("Local port: ");
  Serial.println(udp.localPort());
}

unsigned long sendNTPpacket(IPAddress& address)
{
  Serial.println("sending NTP packet...");
  // set all bytes in the buffer to 0
  memset(packetBuffer, 0, NTP_PACKET_SIZE);

  // Initialize values needed to form NTP request
  // (see URL above for details on the packets)
  packetBuffer[0] = 0b11100011;   // LI, Version, Mode
  packetBuffer[1] = 0;     // Stratum, or type of clock
  packetBuffer[2] = 6;     // Polling Interval
  packetBuffer[3] = 0xEC;  // Peer Clock Precision
  // 8 bytes of zero for Root Delay & Root Dispersion
  packetBuffer[12]  = 49;
  packetBuffer[13]  = 0x4E;
  packetBuffer[14]  = 49;
  packetBuffer[15]  = 52;

  // all NTP fields have been given values, now
  // you can send a packet requesting a timestamp:

  udp.beginPacket(address, 123); //NTP requests are to port 123
  udp.write(packetBuffer, NTP_PACKET_SIZE);
  udp.endPacket();
}

void loop(void)
{
  Serial.println("Requesting temperatures...");
  sensors.requestTemperatures();
  t = sensors.getTempCByIndex(0);
  Serial.print("Temperature for Device 1 is: ");
  Serial.println(t);
  
  char hours, minutes, seconds;
  //get a random server from the pool
  WiFi.hostByName(ntpServerName, timeServerIP);

  sendNTPpacket(timeServerIP); // send an NTP packet to a time server
  // wait to see if a reply is available
  delay(5000);

  int cb = udp.parsePacket();
  if (!cb) {
    Serial.println("no packet yet");
  }
  else {
    Serial.print("packet received, length=");
    Serial.println(cb);
    // We've received a packet, read the data from it
    udp.read(packetBuffer, NTP_PACKET_SIZE); // read the packet into the buffer

    //the timestamp starts at byte 40 of the received packet and is four bytes,
    // or two words, long. First, esxtract the two words:

    unsigned long highWord = word(packetBuffer[40], packetBuffer[41]);
    unsigned long lowWord = word(packetBuffer[42], packetBuffer[43]);
    // combine the four bytes (two words) into a long integer
    // this is NTP time (seconds since Jan 1 1900):
    unsigned long secsSince1900 = highWord << 16 | lowWord;

    // Unix time starts on Jan 1 1970. In seconds, that's 2208988800:
    const unsigned long seventyYears = 2208988800UL;
    // subtract seventy years:
    unsigned long epoch = secsSince1900 - seventyYears;

    // print the hour, minute and second:
    minutes = ((epoch % 3600) / 60);
    minutes = minutes + MM; //Add UTC Time Zone
    hours = (epoch  % 86400L) / 3600;
    if (minutes > 59)
    {
      hours = hours + HH + 1; //Add UTC Time Zone
      minutes = minutes - 60;
    }
    else
    {
      hours = hours + HH;
    }

    seconds = (epoch % 60);

    timenow = String(hours, DEC);
    timenow += ':';
    if (minutes < 10) timenow += "0";
    timenow += String(minutes, DEC);
    timenow += ':';
    if (seconds < 10) timenow += "0";
    timenow += String(seconds, DEC);

    Serial.print("The UTC time is ");       // UTC is the time at Greenwich Meridian (GMT)
    Serial.print(hours, DEC); // print the hour (86400 equals secs per day)
    Serial.print(':');

    if ( minutes < 10 ) {
      // In the first 10 minutes of each hour, we'll want a leading '0'
      Serial.print('0');
    }
    Serial.print(minutes, DEC); // print the minute (3600 equals secs per minute)
    Serial.print(':');

    if ( seconds < 10 ) {
      // In the first 10 seconds of each minute, we'll want a leading '0'
      Serial.print('0');
    }
    Serial.println(seconds, DEC); // print the second
  }
  // END of time request

  //Dweet content
  Serial.print("Connecting to ");
  Serial.println(host);

  WiFiClient client;
  const int httpPort = 80;
  if (!client.connect(host, httpPort)) {
    Serial.println("Connection failed");
    return;
  }

  String url = "/dweet/for/";
  url += thing;
  url += "?";
  url += thing_content;
  url += "=";
  url += t;
  url += "&";
  url += thing_content1;
  url += "=";
  url += timenow;

  Serial.print("Requesting URL: ");
  Serial.println(url);

  // This will send the request to the server
  client.print(String("GET ") + url + " HTTP/1.1\r\n" +
               "Host: " + host + "\r\n" +
               "Connection: close\r\n\r\n");
  int timeout = millis() + 5000;
  while (client.available() == 0) {
    if (timeout - millis() < 0) {
      Serial.println(">>> Client Timeout !");
      client.stop();
      return;
    }
  }

  while (client.available()) {
    String line = client.readStringUntil('\r');
    Serial.print(line);
  }

  Serial.println();
  Serial.println("closing connection");

  if (t < 12) {
    if (sendEmail(t)) {
      Serial.println(F("Email sent"));
      delay(600000); //wait five minutes
    }
    else Serial.println(F("Email failed"));
  };

  delay(60000);
}

byte sendEmail(float t) {
  WiFiClientSecure client;
  if (client.connect("smtp.gmail.com", 465) == 1) Serial.println(F("connected"));
  else {
    Serial.println(F("connection failed"));
    return 0;
  }

  if (!eRcv(client)) return 0;
  Serial.println(F("--- Sending EHLO")); client.println("EHLO 1.2.3.4"); if (!eRcv(client)) return 0;
  Serial.println(F("--- Sending login")); client.println("AUTH LOGIN"); if (!eRcv(client)) return 0;
  Serial.println(F("--- Sending User base64")); client.println(user_base64); if (!eRcv(client)) return 0;
  Serial.println(F("--- Sending Password base64")); client.println(user_password_base64); if (!eRcv(client)) return 0;
  Serial.println(F("--- Sending From")); client.println(from_email); if (!eRcv(client)) return 0;
  Serial.println(F("--- Sending To")); client.println(to_email); if (!eRcv(client)) return 0;
  Serial.println(F("--- Sending DATA")); client.println(F("DATA")); if (!eRcv(client)) return 0;
  client.println(F("Subject: Temperature warning\r\n"));
  client.println(F("Temperature from sensor:\n"));
  client.println(t);
  client.println(F("."));
  if (!eRcv(client)) return 0;
  Serial.println(F("--- Sending QUIT"));
  client.println(F("QUIT"));
  if (!eRcv(client)) return 0;
  client.stop();
  return 1;
}

byte eRcv(WiFiClientSecure client)
{
  byte respCode;
  byte thisByte;
  int loopCount = 0;

  while (!client.available()) {
    delay(1);
    loopCount++;
    if (loopCount > 10000) {
      client.stop();
      Serial.println(F("\r\nTimeout"));
      return 0;
    }
  }

  respCode = client.peek();
  while (client.available())
  {
    thisByte = client.read();
    Serial.write(thisByte);
  }
  if (respCode >= '4')return 0;
  return 1;
}
